replaces text in supplied file

*flags:*

-pattern  - search pattern (currently only bla="*" is supported) 

-target   - target file

-ignore   - file with ignored words, new line separated

-default  - default value to be replaced with


**Example:**

./replace -pattern align="*" -target myfile.html -ignore ignore.txt -default right

will replace all align="left" with align="right"