package main

import (
	"flag"
	"io/ioutil"
	"log"
	"strings"
)

type input struct {
	searchPattern       string
	targetFilePath      string
	ignoreWordsFilePath string
	newValue            string
}

// run like:
// go run main.go -pattern myxmlattribute=\" -target test_file.txt -ignore ignore.txt -default THIS_IS_HOROSHO
func main() {
	programInput := getInput()
	targetFile, err := ioutil.ReadFile(programInput.targetFilePath)
	if err != nil {
		log.Fatal("can't open target file")
	}
	content := []rune(string(targetFile))

	ignoredFile, err := ioutil.ReadFile(programInput.ignoreWordsFilePath)
	if err != nil {
		log.Fatal("can't open ignore file")
	}

	ignoredWords := getIgnoredWords(ignoredFile)
	wordsTobeReplaced := getWordTobeReplaced(content, ignoredWords, programInput.searchPattern)

	targetStr := string(targetFile)
	for w := range wordsTobeReplaced {
		targetStr = strings.ReplaceAll(targetStr, w, programInput.newValue)
	}
	err = ioutil.WriteFile(programInput.targetFilePath, []byte(targetStr), 0666)
	if err != nil {
		log.Fatal(err)
	}
}

func getInput() (programInput input) {
	const notProvided = "notProvided"
	pattern := flag.String("pattern", notProvided, "search pattern (currently only bla=\" is supported)")
	target := flag.String("target", notProvided, "target file")
	ignore := flag.String("ignore", notProvided, "file with ignored words, new line separated")
	newValue := flag.String("default", notProvided, "default value to be replaced with")
	flag.Parse()

	programInput.searchPattern = *pattern
	programInput.targetFilePath = *target
	programInput.ignoreWordsFilePath = *ignore
	programInput.newValue = *newValue

	if programInput.searchPattern == notProvided ||
		programInput.targetFilePath == notProvided ||
		programInput.ignoreWordsFilePath == notProvided ||
		programInput.newValue == notProvided {
		log.Fatal("flags not provided, example: ./replace -pattern align=\"*\" -target myfile.html -ignore ignore.txt -default right")
	}
	return
}

func getIgnoredWords(input []byte) (output map[string]struct{}) {
	output = make(map[string]struct{})
	var temp []byte
	for _, b := range input {
		if b == '\n' {
			output[string(temp)] = struct{}{}
			temp = temp[:0]
		} else {
			temp = append(temp, b)
		}
	}
	// if file doesn't end with a new line - then add last one
	if len(temp) > 0 {
		output[string(temp)] = struct{}{}
	}
	return
}

func getWordTobeReplaced(content []rune, ignored map[string]struct{}, pattern string) (output map[string]struct{}) {
	output = make(map[string]struct{})
	searchPattern := []rune(pattern)
	var temp []rune
	// searcing for abc="value", the last double quotes is endPattern
	endPattern := '"'
	k := 0
	patternMatched := false
	for i := 0; i < len(content); i++ {
		if patternMatched {
			// looping inside mathced pattern
			if content[i] == endPattern {
				// exiting pattern part - a complete word extracted
				// check ignored words vocabulary
				patternMatched = false
				_, ok := ignored[string(temp)]
				if !ok {
					output[string(temp)] = struct{}{}
				}
				temp = temp[:0]
			} else {
				temp = append(temp, content[i])
				continue
			}
		}
		if k == len(searchPattern) && content[i] != endPattern {
			// pattern matched here, need to get the word until 'endPattern' e.g. "
			patternMatched = true
			k = 0
			temp = append(temp, content[i])
		} else {
			// looping across whole file, seaching for a starting pattern
			if content[i] == searchPattern[k] {
				k++
			} else {
				k = 0
			}
		}
	}
	return
}
